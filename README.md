This repository provides Jupyter Notebooks to accompany my blog which can be found at: https://blogs.ed.ac.uk/mnaylor/

## Python Examples

- <a href="https://git.ecdf.ed.ac.uk/mnaylor/blog-post-notebooks/blob/master/2018-October/scrapingExamples.ipynb">Loading and scraping data from the web</a>
- Plotting data on static maps with Python [TODO]
- Analysing Geophysical timeseries [TODO]

## Statistical Seismology
- Loading and plotting properties of an earthquake catalogue [TODO]
- Analysis of the Gutenberi-Richter Relation: Estimating the b-value [TODO]
- Analysis of the Gutenberi-Richter Relation: Handling incompleteness [TODO]
- Analysis of the Gutenberi-Richter Relation: Analysing real data [TODO]